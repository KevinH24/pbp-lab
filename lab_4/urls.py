from django.urls import path
from lab_4.views import *
urlpatterns = [
    path('', index, name='index'),
    path('add-note/', add_note, name= 'add_notes'),
    path('note-list/', note_list, name = 'note_list')
]