import 'package:flutter/material.dart';
import'package:lab_6/Palette.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mutuals Randomize',
      theme: ThemeData(
        fontFamily: 'Montagu',
        primarySwatch: Palette.kToDark,
      ),
        home: Scaffold(
          appBar: AppBar(
            toolbarHeight: 100,
            title: Image.asset(
                'assets/images/logo.png',
                 height: 115,
                 width:  115,
            ),
            centerTitle: true,
          ),
            backgroundColor: Colors.white,
          body: MyCardWidget(),
        ),
    );
  }


}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
@override
  State<MyHomePage> createState() => _MyHomePageState();
}
class MyCardWidget extends StatelessWidget {
  var style;

  MyCardWidget({Key? key}) : super(key: key);
  

  @override
Widget build(BuildContext context) {
  return Center(
      child: Container(
        width: 300,
        height: 400,
        padding: new EdgeInsets.all(4.0),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7.0),
            side: BorderSide(color: Colors.black, width: 2),
          ),
          color: Colors.lime,
          elevation: 10,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
               const ListTile(
                title: Center(
                  child: Text(
                    'Kevin Heryanto',
                    style: TextStyle(fontSize: 30.0),
                    textAlign: TextAlign.center,
                ),
                ),
                subtitle: Text(
                    'KH24',
                    style: TextStyle(fontSize: 20.0)
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Male | May 6, 2002 | Jakarta',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Email: Kevinheryanto24@gmail.com',
                  style: TextStyle(color: Colors.black),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Line: KH24',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Instagram :KH24',
                  style: TextStyle(color: Colors.black),
                ),
              ),
              ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: const Text('Randomize Again!'),
                    onPressed: () {/* ... */},
                    padding: EdgeInsets.fromLTRB(10, 10, 120, 10),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
  );
}
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BottomNavigationBar Demo'),
      ),
      body: Container(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.perm_device_information_sharp),
              title: Text('About'),
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.people_alt_outlined),
              title: Text('Friends'),
              backgroundColor: Colors.blue
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_box),
              title: Text('Account'),
              backgroundColor: Colors.blue
          ),
        ]
      ),
    );
// the App.build method, and use it to set our appbar title.


}
}
