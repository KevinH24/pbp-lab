# Lab 6: Flutter Widget

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Odd Semester 2021/2022

---

*Untuk membaca README ini dalam bahasa Indonesia, [klik di sini](README.md).*

## Learning Objectives

After completing this tutorial, students are expected to be able to:

- Understand the structure, components, and lifecycle of the mobile platform
  using Flutter
- Implement a simple mobile app on top of the Flutter framework
- Make use of widgets to build the interface structure of a mobile application

## Tasks

You are tasked to create a new app in this project named `lab_6`, which is a
project on top of the Flutter framework that displays a page that was
previously made for the web platform. Make sure that the page contains:

1. A widget that shows a basic element, e.g. `Button`, `Text`, etc.
2. A widget that defines the position, margin, padding, e.g. `Padding`,
   `Scaffolding`, etc.
3. A widget that helps you implement the theme, visual, or structural element,
   e.g. `Theme`.

## Checklist

1. [x] Go to directory `lab_6`in root directory (`pbp-lab`).
2. [x] Run `flutter create .` to initiate Flutter in your `lab_6` directory. If
   you would like to try `lab_6` boilerplate, just run `flutter run`.
3. [x] Modify `lib/main.dart` rename to your application name
4. [x] Modify `lib/widgets/main_drawer.dart` to add more menus (if required)
5. [x] Modify screens as required to be as similar to your previous web
   application. Code samples can be found at `lib/screens` directory.
   1. [x] I've already use basic widget in my modified screen
   2. [x] I've already use positioning widget in my modified screen
   3. [x] I've already use theme widget in my modified screen
6. [x] Try the application that you have built in this lab using a web browser,
   a mobile device, or an emulator, just run `flutter run`.

## Referensi

1. https://www.freecodecamp.org/news/an-introduction-to-flutter-the-basics-9fe541fd39e2/
2. http://livre21.com/LIVREF/F6/F006145.pdf
3. Official Flutter Docs: https://flutter.io/docs/
4. macOS Setup Guide: https://flutter.io/setup-macos
5. Windows Setup Guide: https://flutter.io/setup-windows
6. Linux Setup Guide: https://flutter.io/setup-linux
7. Visual Studio Code: https://code.visualstudio.com/
8. Visual Studio Code Flutter Extension:
   https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter
9. Android Studio: https://developer.android.com/studio/
