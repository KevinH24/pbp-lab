import 'package:flutter/material.dart';
import './login.dart';
import './Palette.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MutualsLogin(),
    );
  }
}

class MutualsLogin extends StatefulWidget {
  @override
  _MutualsLoginState createState() => _MutualsLoginState();
}

class _MutualsLoginState extends State<MutualsLogin> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Mutuals Login',
        theme: ThemeData(
          fontFamily: 'Rubik',
          primarySwatch: Palette.kToDark,
        ),
      home: Scaffold(
        appBar: AppBar(
          toolbarHeight: 100,
          title: Image.asset(
            'assets/images/logo.png',
            height: 115,
            width:  115,
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Text(
                  "Sign in and let's be friends!",
                  textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 50.0),
                ),
              ),
            ),
            SizedBox(height: 30),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: TextField(
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                    ),
              ),
            ),
            SizedBox(height: 20),
            Padding(
              //padding: const EdgeInsets.only(
                  //left: 10.0, right: 10.0, top: 15, bottom: 0),
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: TextField(

                obscureText: true,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                ),
              ),
            ),
            SizedBox(height: 30),
            Container(
              height: 50,
              width: 150,
              decoration: BoxDecoration(
                  color: Colors.yellowAccent,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
              child: FlatButton(
                onPressed: (){
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => HomePage()));
                },
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ),
            SizedBox(height: 30),
            FlatButton(
              onPressed: (){
              },
              child: Text(
                "Haven't created an account?",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ],
        ),
      ),
    ),
    );
  }
}
