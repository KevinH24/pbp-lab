import 'package:flutter/material.dart';
import './Palette.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          fontFamily: 'Rubik',
          primarySwatch: Palette.kToDark,
        ),
      home: Scaffold(
        appBar: AppBar(
          toolbarHeight: 100,
          title: Image.asset(
            'assets/images/logo.png',
            height: 115,
            width:  115,
          ),
          centerTitle: true,
        ),
        body: Center(
          child: Image.asset(
            'assets/images/download.png',
            height: 115,
            width:  115,
          ),
        ),
      ),
    );
  }
}
