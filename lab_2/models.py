from django.db import models
class Note (models.Model):
    to = models.CharField(max_length=30)
    from_note = models.CharField(max_length=30)
    title_note = models.CharField(max_length=30)
    message = models.TextField()
# Create your models here.
